package message

type TextMessage struct {
	MsgType string `json:"msgtype"`
	Text    *Text  `json:"text"`
	At      *At    `json:"at"`
}

func NewText(content string) *Text {
	return &Text{content}
}

type Text struct {
	Content string `json:"content"`
}

func NewTextMessage(text *Text, at *At) *TextMessage {
	return &TextMessage{
		MsgType: "text",
		Text:    text,
		At:      at,
	}
}
