package message

type MarkdownMessage struct {
	MsgType  string    `json:"msgtype"`
	Markdown *Markdown `json:"markdown"`
	At       *At
}

func NewMarkdown(title, text string) *Markdown {
	return &Markdown{
		Title: title,
		Text:  text,
	}
}

type Markdown struct {
	Title string `json:"title"`
	Text  string `json:"text"`
}

func NewMarkdownMessage(markdown *Markdown, at *At) *MarkdownMessage {
	return &MarkdownMessage{
		MsgType:  "markdown",
		Markdown: markdown,
		At:       at,
	}
}
