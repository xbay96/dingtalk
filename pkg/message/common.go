package message

type At struct {
	AtMobiles []string `json:"atMobiles"`
	IsAtAll   bool     `json:"isAtAll"`
}

func NewAt(atMobiles []string, isAtAll bool) *At {
	return &At{
		AtMobiles: atMobiles,
		IsAtAll:   isAtAll,
	}
}
