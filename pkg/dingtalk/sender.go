package dingtalk

import (
	"bytes"
	"encoding/json"
	"net/http"
)

func SendMessage(webhookURL string, msg interface{}) error {
	messageBytes, err := json.Marshal(msg)
	if err != nil {
		return err
	}

	resp, err := http.Post(webhookURL, "application/json", bytes.NewBuffer(messageBytes))
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	return nil
}
