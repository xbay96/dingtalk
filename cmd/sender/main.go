package main

import (
	"fmt"
	"gitee.com/xbay96/dingtalk/pkg/dingtalk"
	"gitee.com/xbay96/dingtalk/pkg/message"
	"log"
	"time"
)

func main() {

	//sendText
	webhookURL := "https://oapi.dingtalk.com/robot/send?access_token="
	text := message.NewText(fmt.Sprintf("dingtalk send test %s", time.Now()))
	at := message.NewAt([]string{""}, false)
	textMsg := message.NewTextMessage(text, at)
	if err := dingtalk.SendMessage(webhookURL, textMsg); err != nil {
		log.Fatalf("Failed to send message: %v", err)
	}
	//sendMarkdown
	title := "任务执行失败告警"
	markdownContent := `### 这是一条告警信息
#### 任务执行失败告警
##### 任务名称：测试任务
##### 任务ID：123456
##### 执行人：张三
##### 执行时间：2021-01-01 12:00:00
##### 执行结果：失败
##### 失败原因：执行超时
##### 失败详情：执行超时`
	markdown := message.NewMarkdown(title, markdownContent)
	markdownSendMessage := message.NewMarkdownMessage(markdown, at)
	if err := dingtalk.SendMessage(webhookURL, markdownSendMessage); err != nil {
		log.Fatalf("Failed to send message: %v", err)
	}
}
